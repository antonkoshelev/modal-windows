const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development';

const config = {
    mode: 'development',
    entry: {
        index: ['babel-polyfill', './src/index.js']
    },
    output: {
        filename: '[name].[hash].js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['node_modules'],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.(ttf|woff|woff2|eot)$/,
                use: 'file-loader',
            },
        ],
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: './public/index.html',
            minify: {
              collapseWhitespace: !isDev,
            },
        }),
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: path.resolve(__dirname, 'public', 'assets'),
                    to: path.resolve(__dirname, 'dist', 'assets'),
                },
            ]
        }),
        new webpack.DefinePlugin({
            __DEV__: isDev,
            __PROD__: !isDev,
        }),
    ],
    devServer: {
        port: 8071,
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
        }
    },
    devtool: 'eval-source-map',
};

module.exports = config;