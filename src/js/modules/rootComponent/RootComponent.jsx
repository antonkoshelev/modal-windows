import React from 'react';
import {
    WrapperApp,
    GlobalStyle,
    ModalWindowContainer,
} from './styledComponents';
import ModalManager from '../../managers/modalManager/index';
import CustomButton from '../../customComponents/customButton/CustomButton';

const RootComponent = (props) => {
    const {
        config,
        openModalWindow,
    } = props;

    return (
        <>
            <WrapperApp>
                {
                    config.mainButtons.map(item => (
                        <CustomButton
                            key={item}
                            textValue={item}
                            isBlueButton={item === 'Open'}
                            handleOnClick={() => openModalWindow(item)}
                        />
                    ))
                }
            </WrapperApp>
            <ModalWindowContainer>
                <ModalManager/>
            </ModalWindowContainer>
            <GlobalStyle/>
        </>
    );
};

export default React.memo(RootComponent);