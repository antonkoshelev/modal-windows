import styled, { createGlobalStyle } from 'styled-components';
import Inter from '../../../../public/assets/fonts/Inter-Regular.ttf'

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Inter';
    src: url(${Inter}) format('truetype');
  }
  
  body {
    height: 100vh;
    margin: 0;
    padding: 0;
    overflow: hidden;
    font-family: Inter, sens-serif;
  }

  #root {
    height: 100%;
  }

  .blur-on {
    background: #070F18;
    opacity: 0.4;
    pointer-events: none;
  }
`;

export const WrapperApp = styled.div`
  width: 100vw;
  height: 100vh;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 21px;
`;

export const ModalWindowContainer = styled.div`
  width: 100vw;
  height: 100vh;
  position: absolute;
`;