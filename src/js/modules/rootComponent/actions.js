import ActionTypes from '../../constants/actionTypes'

export const openModalWindow = payload => ({ type: ActionTypes.OPEN_MODAL_WINDOW, payload })