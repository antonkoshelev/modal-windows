import Component from './RootComponent';
import { connect } from 'react-redux';
import * as selectors from './selectors';
import * as actions from './actions';

export const mapStateToProps = state => ({
    config: selectors.getConfig(state),
})

export const mapDispatchToProps = dispatch => ({
    openModalWindow: payload => dispatch(actions.openModalWindow(payload)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Component);