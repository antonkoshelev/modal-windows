import {
    all,
    call,
} from 'redux-saga/effects';
import modalManagerSaga from '../managers/modalManager/saga'

export function getSagasList() {
    const managerSagas = {
        modal: modalManagerSaga,
    };

    return [
        ...filterImportedSagas(managerSagas),
    ]
}

export function filterImportedSagas(sagas) {
    let importedSagaList = [];

    for (let key in sagas) {
        importedSagaList.push(sagas[key]);
    }

    return importedSagaList;
}

export default function* watchRootSaga() {
    const sagasList = yield call(getSagasList);

    if (!sagasList) {
        console.error("can't create sagas!!");
        return false;
    }

    yield all(sagasList.map(saga => call(saga)));
}