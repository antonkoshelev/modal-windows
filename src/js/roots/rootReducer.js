import { combineReducers } from 'redux';
import modalManagerReducer from '../managers/modalManager/reducer';
import configPlatformReducer from '../config/reducer';

export function filterImportedReducers(reducers) {
    let importedReducers = {};

    for (let key in reducers) {
        importedReducers = {
            ...importedReducers,
            [key]: reducers[key],
        };
    }

    return importedReducers;
}

export const getRootReducer = () => {
    const importedMangers = {
        modal: modalManagerReducer,
    };

    return combineReducers({
        config: configPlatformReducer,
        ...filterImportedReducers(importedMangers),
    })
}