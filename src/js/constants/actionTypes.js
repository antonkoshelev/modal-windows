import keyMirror from 'key-mirror';

export default keyMirror({
    OPEN_MODAL_WINDOW: null,
    ON_CLOSE_MODAL_WINDOW: null,
    CHANGE_MODAL_WINDOW_STATE: null,
});