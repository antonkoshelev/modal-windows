export default {
    mainButtons: ['Edit', 'Open'],
    createCostumerWindow: {
        select: {
            name: 'primaryPaymentMethod',
            label: 'Primary payment method',
            options: [
                { value: 'chocolate', label: 'Privat24' },
                { value: 'strawberry', label: 'PayPal' },
                { value: 'vanilla', label: 'Pioneer' }
            ],
            description: 'Payment and billing:',
            placeholder: 'Choose payment method',
        },
        availableInputs: [
            {
                name: 'customerName',
                labelText: 'Customer name',
                placeholder: 'Enter customer name',
            },
            {
                name: 'customersEIN',
                labelText: 'Customer EIN',
                placeholder: 'Enter customer EIN',
            },
            {
                name: 'notes',
                labelText: 'Notes',
                placeholder: 'Notes visible only to you and your team',
            },
        ],
        availableButtons: ['Cancel', 'Create'],
    },
};