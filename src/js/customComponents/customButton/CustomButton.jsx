import React from 'react';
import { Button } from './styledComponents';

const CustomButton = props => {
    const {
        textValue,
        isBlueButton,
        handleOnClick,
    } = props;

    return (
        <Button
            onClick={handleOnClick}
            isBlueButton={isBlueButton}
        >
            <Button.text>
                {textValue}
            </Button.text>
        </Button>
    );
};

export default React.memo(CustomButton);