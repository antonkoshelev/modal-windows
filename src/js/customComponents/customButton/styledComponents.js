import styled from 'styled-components';

export const Text = styled.span`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 14px;
`;

export const Button = styled.div`
  width: 230px;
  height: 50px;
  align-self: center;
  justify-self: ${props => props.isBlueButton
          ? 'start'
          : 'end'
  };
  background: ${props => props.isBlueButton
          ? '#6367F6'
          : '#FFF'
  };
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 11px 18px;
  border: 1px solid #EDEFF1;
  box-sizing: border-box;
  border-radius: 10px;
  cursor: pointer;
  
  ${Text} {
    color: ${props => props.isBlueButton
            ? '#FFF'
            : '#000'
    }
  }

  &:hover {
    ${props => props.isBlueButton
            ? ''
            : 'background: rgba(0,0,0, 0.1)'
    };
    opacity: 0.8;
  }
`;
Button.text = Text;