import Modal from 'react-modal';
import React from 'react';
import CreateCostumerWindow from './components/createCostumerWindow/CreateCostumerWindow';
import OpenWindow from './components/openWindow/OpenWindow';

let customStyles = {
    overlay: {
        right: 0,
        bottom: 0,
        zIndex: 200,
        position: 'static',
        backgroundColor: 'rgba(0,0,0, 0.5)',
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: 'none',
        zIndex: 199,
        padding: '0',
        overflowY: 'hidden',
        transform: 'translate(-50%, -50%)',
        background: 'none',
        marginRight: '-50%',
        borderRadius: '0 0 20px 20px',
    },
};

const getContent = props => {
    const {
        onClose,
        context,
    } = props;

    switch (context?.subType) {
        case 'Edit':
            return (
                <CreateCostumerWindow onClose={onClose}/>
            );
        case 'Open':
            return (
                <OpenWindow onClose={onClose}/>
            );
        default:
            return null;
    }

}

const ModalManager = (props) => {
    const {
        isShow,
        onClose,
    } = props;

    return (
        <Modal
            style={customStyles}
            isOpen={isShow}
            ariaHideApp={false}
            onRequestClose={onClose}
            shouldCloseOnEsc={true}
            shouldCloseOnOverlayClick={true}>
            {getContent(props)}
        </Modal>
    );
};

export default React.memo(ModalManager);