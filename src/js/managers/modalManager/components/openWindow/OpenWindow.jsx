import React from 'react';
import { Wrapper } from './styledComponents'

const OpenWindow = props => {
    return (
        <Wrapper onClick={props.onClose}/>
    );
};

export default React.memo(OpenWindow);