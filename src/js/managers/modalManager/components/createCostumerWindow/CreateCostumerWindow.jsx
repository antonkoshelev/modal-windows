import React from 'react';
import {
    Main,
    Header,
    Wrapper,
    InputContainer,
    SelectContainer,
} from './styledComponents'
import { useSelector } from 'react-redux';
import CustomButton from '../../../../customComponents/customButton/CustomButton';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

const schema = yup.object().shape({
    customerName: yup.string().required('Customer name is required').max(24, 'Customer name must be at least 24 characters'),
    customersEIN: yup.number().typeError('EIN must be a number').positive('EIN must be a positive number').integer('EIN must be a positive number integer'), //TODO потратил достаточно времени, но так и не понял как сделать его не обязательным при отправке формы. Хотел бы обсудить
    notes: yup.string(),
    primaryPaymentMethod: yup.object().shape({
        label: yup.string(),
        value: yup.string()
    }),
})

const handleOnConfirm = (data, reset) => {
    console.log('sdfghjk', data)
    reset();
}

const CreateCostumerWindow = props => {
    const createCostumerWindowConfig = useSelector(state => state.config.createCostumerWindow)
    const {
        register,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm({
        mode: 'onBlur',
        resolver: yupResolver(schema),
    });
    const {
        name,
        label,
        options,
        description,
        placeholder,
    } = createCostumerWindowConfig.select;

    return (
        <Wrapper>
            <Wrapper.Header>
                <Header.text>
                    {'Create new customer'}
                </Header.text>
                <Header.closeButton onClick={props.onClose}/>
            </Wrapper.Header>
            <Wrapper.Main onSubmit={handleSubmit(handleOnConfirm)}>
                {
                    createCostumerWindowConfig.availableInputs.map(item => {
                        const {
                            name,
                            labelText,
                            placeholder,
                        } = item;
                        const Label = InputContainer[`${name}Label`];
                        const Input = InputContainer[`${name}Input`];
                        const Error = InputContainer[`${name}ErrorText`];

                        return (
                            <Main.InputContainer key={name}>
                                <Label isRequired={name === 'customerName'}>
                                    {labelText}
                                </Label>
                                <Input
                                    {...register(name)}
                                    placeholder={placeholder}
                                />
                                {
                                    name !== 'notes'
                                        ? <Error>
                                            {errors?.[name]?.message}
                                        </Error>
                                        : null
                                }
                            </Main.InputContainer>
                        )
                    })
                }
                <Main.DescriptionSelectContainer>
                    {description}
                </Main.DescriptionSelectContainer>
                <Main.SelectContainer>
                    <SelectContainer.selectLabel>
                        {label}
                    </SelectContainer.selectLabel>
                    <SelectContainer.reactSelect
                        {...register(name)}
                        options={options}
                        placeholder={placeholder}
                    />
                    <InputContainer.selectErrorText>
                        {errors?.[name]?.message}
                    </InputContainer.selectErrorText>
                </Main.SelectContainer>
            </Wrapper.Main>
            <Wrapper.Footer>
                {
                    createCostumerWindowConfig.availableButtons.map(item => {
                        const isCreateButton = item === 'Create'

                        return (
                            <CustomButton
                                key={item}
                                textValue={item}
                                isBlueButton={isCreateButton}
                                handleOnClick={isCreateButton
                                                    ? handleSubmit(data => handleOnConfirm(data, reset))
                                                    : props.onClose
                                                }
                            />
                        )
                    })
                }
            </Wrapper.Footer>
        </Wrapper>
    );
};

export default React.memo(CreateCostumerWindow);