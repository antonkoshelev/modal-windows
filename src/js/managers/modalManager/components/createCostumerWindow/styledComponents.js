import styled from 'styled-components';
import Select from 'react-select';

const inputsStyle = `
  width: 100%;
  padding: 8px 16px;
  background: #F9F9FA;
  border: 1px solid #CFD6DE;
  border-radius: 10px;
  margin-top: 2px;
  box-sizing: border-box;
  outline: none;
  font-weight: 400;
  font-size: 14px;
  line-height: 28px;

  &::placeholder {
    color: #CFD6DE;
  }
`;

export const Wrapper = styled.div`
  width: 924px;
  height: 668px;
  display: grid;
  grid-template-rows: 64px 473px 131px;
  background: #FFFFFF;
  border-radius: 0 0 20px 20px;
`;

export const Header = styled.div`
  width: 100%;
  background: #F9F9FA;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Main = styled.div`
  width: 450px;
  height: 393px;
  justify-self: center;
  align-self: center;
`;

export const Footer = styled.div`
  width: 100%;
  border-top: 1px solid #EDEFF1;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 21px;
`;

export const Text = styled.span`
  font-style: normal;
  font-weight: 700;
  font-size: 20px;
  line-height: 28px;
  padding-left: 2px;
`;

export const CloseButton = styled.div`
  height: 24px;
  width: 24px;
  position: absolute;
  right: 20px;
  background-image: url('./assets/close-icon.svg');
  cursor: pointer;

  &:hover {
    filter: brightness(90%);
  }
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: start;
`;

export const SelectContainer = styled(InputContainer)``;

export const DescriptionSelectContainer = styled.div`
  height: 40px;
  display: flex;
  justify-content: start;
  align-items: start;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 28px;
  padding-left: 2px;
  margin-top: 3px;
  color: #5A607D;
`;

export const Label = styled.label`
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 16px;
  color: #000000;
  padding-left: 2px;
  ${props => props?.isRequired
          ? `&:after {
                    content:" *";
                    color: red;
                  }`
          : ''
  };
`;

export const Input = styled.input`
  height: 52px;
  ${inputsStyle}
`;

export const TextArea = styled.textarea`
  height: 82px;
  resize: none;
  ${inputsStyle}
`;

export const ErrorText = styled.p`
  height: 15px;
  font-size: 12px;
  color: red;
  padding-left: 2px;
  margin: 0 0 4px 0;
`;

export const ReactSelect = styled(Select)`
  width: 100%;

  & > div {
    ${inputsStyle}
    
    & > div {
      padding: 0;

      & + div {
        span {
          display: none;
        }
        
        & > div {
          padding: 5px;
          
          & > svg {
            height: 9px;
            width: 10px;
            fill: #000;
            stroke: #000;
            stroke-width: 5px;
          }
        }
      }

      #react-select-2-placeholder {
        color: #CFD6DE;
        margin: 0;
      }
    }
  }
`;

Wrapper.Header = Header;
Header.text = Text;
Header.closeButton = CloseButton;
Wrapper.Main = Main;
Wrapper.Footer = Footer;
Main.InputContainer = InputContainer;
Main.SelectContainer = SelectContainer;
Main.DescriptionSelectContainer = DescriptionSelectContainer;
InputContainer.customerNameLabel = Label;
InputContainer.customerNameInput = Input;
InputContainer.customerNameErrorText = ErrorText;
InputContainer.customersEINLabel = Label;
InputContainer.customersEINInput = Input;
InputContainer.customersEINErrorText = ErrorText;
InputContainer.notesLabel = Label;
InputContainer.notesInput = TextArea;
InputContainer.notesErrorText = ErrorText;
SelectContainer.selectLabel = Label;
SelectContainer.reactSelect = ReactSelect;
InputContainer.selectErrorText = ErrorText;