import ActionTypes from '../../constants/actionTypes'

export const onCloseModalWindow = () => ({type: ActionTypes.ON_CLOSE_MODAL_WINDOW})
export const changeModalWindowState = payload => ({type: ActionTypes.CHANGE_MODAL_WINDOW_STATE, payload})