export const getModalContext = state => state.modal.context;
export const getModalIsShowState = state => state.modal.isShow;
