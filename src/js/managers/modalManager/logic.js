export const toggleBlurPlatform = toggle => {
    const container = document.querySelector('#root');
    container.classList.toggle('blur-on', toggle);
    const body = document.querySelector('body');
    body.classList.toggle('overflow-hidden', toggle);
}