import ActionTypes from '../../constants/actionTypes';

const initialState = {
    isShow: false,
    context: null,
};

export default (state = initialState, action) => {
    switch(action.type) {
        case ActionTypes.CHANGE_MODAL_WINDOW_STATE:
            return {
                ...action.payload,
            }
        default:
            return state;
    }
}