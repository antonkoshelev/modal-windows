import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import {
    compose,
    createStore,
    applyMiddleware,
} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { getRootReducer } from './js/roots/rootReducer';
import rootSaga from './js/roots/rootSaga';
import RootComponent from './js/modules/rootComponent/index';
import configPlatform from './js/config/configPlatform';

const initialState = {
    config: configPlatform,
}
const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    getRootReducer(),
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(rootSaga);

if (__DEV__) {
    window.store = store;
}

render(
    <Provider store={store}>
        <RootComponent/>
    </Provider>,
    document.getElementById('root')
)

